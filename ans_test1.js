var assert = require('assert');
const test = [
 {
   "p": 73,
   "r": 98,
   "t": 11
 },
 {
   "p": 25,
   "r": 37,
   "t": 77
 },
 {
   "p": 34,
   "r": -44,
   "t": 69
 },
 {
   "p": 86,
   "r": 25,
   "t": 27
 },
 {
   "p": 27,
   "r": -38,
   "t": 11
 },
 {
   "p": 4,
   "r": 11,
   "t": 66
 }
]
function calculate(arrayPRT)
{
   if(arrayPRT.length == 0)
   {
   return {}
   }
   return arrayPRT.map(obj=>{
	const {p,r,t} = obj;
	const ans = (p*r*t)/100;
	return ans.toFixed(2);
}).filter(item=>item>0)
};
const resultArray = calculate(test);
console.log(resultArray);
assert.deepEqual(resultArray,['786.94','712.25', '580.50', '29.04']);
