function maporder(array,order,key)
{
  array.sort(function(a,b){
  var A=a[key],B=b[key];
  if(order.indexOf(A) > order.indexOf(B))
  {
	return 1; 
  }
  else
  {
	return -1;
  }
});
return array;
};

var arr1 = ['0e089d7ee981bec654acb4a678cef7e6', '7adefed65ed36cb257a5e44bc0ad9919', '64450e7b290f8abcfb070a27d5eaf202'];
var arr2 = [{
   "hashKey" : "7adefed65ed36cb257a5e44bc0ad9919",
   "updatedAt" : new Date("2020-02-02T07:56:44.731Z"),
   "sizeInBytes" : 127543,
   "storePath" : "arxiv/pdf/astro-ph0001011.pdf",
   "createdAt" : new Date("2020-02-02T07:56:44.731Z")
}, {
   "hashKey" : "0e089d7ee981bec654acb4a678cef7e6",
   "updatedAt" : new Date("2020-02-07T07:56:44.731Z"),
   "sizeInBytes" : 197563,
   "storePath" : "arxiv/pdf/astro-ph0001018.pdf",
   "createdAt" : new Date("2020-02-07T07:56:44.731Z")
}, {}, {
   "updatedAt" : new Date("2020-02-06T07:56:44.731Z"),
   "sizeInBytes" : 15705,
   "storePath" : "arxiv/pdf/astro-ph0001010.pdf",
   "createdAt" : new Date("2020-02-06T07:56:44.731Z")
}];

ordered_array = maporder(arr2,arr1,'hashKey');
console.log('Ordered:',JSON.stringify(ordered_array,null,2));
